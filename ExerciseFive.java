// Different packages of ground beef have different percentages of fat and
// different costs per pound. Write a program that asks the user for:
//      1. The price per pound of package &quot;A&quot;
//      2. The percent lean in package &quot;A&quot;
//      3. The price per pound of package &quot;B&quot;
//      4. The percent lean in package &quot;B&quot;
// The program then calculates the cost per pound of lean (non-fat) beef for each
// package and writes out which is the best value.
// Price per pound package A: 2.89
// Percent lean package A: 85
// Price per pound package B: 3.49
// Percent lean package B: 93
// Package A cost per pound of lean:3.4
// Package B cost per pound of lean:3.752688
// Package A is the best value
// Assume that the two packages will not come out equal in value.

import java.util.Scanner;
public class ExerciseFive
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Price per pound package A: ");
        double priceA = scan.nextDouble();
        System.out.println("Percent lean package A: ");
        int percentA = scan.nextInt();
        
        System.out.println("Price per pound package B: ");
        double priceB = scan.nextDouble();
        System.out.println("Percent lean package B: ");
        int percentB = scan.nextInt();
        
        double leanCostA = priceA * (. 01 * percentA);
        double leanCostB = priceB * (. 01 * percentB);
        
        System.out.println("Package A cost per pound of lean: " + leanCostA);
        System.out.println("Package B cost per pound of lean: " + leanCostB);
        
        if (leanCostA > leanCostB) {
            System.out.println("Package A is the best value!");
        } else {
            System.out.println("Package B is the best value!");
        }
        
    }
}