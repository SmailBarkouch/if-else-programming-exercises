// Write a program that determines the price of a movie ticket. The program asks
// for the customer&#39;s age and for the time on a 24-hour clock (where noon is 1200
// and 4:30 pm is 1630). The normal adult ticket price is $8.00, however the adult
// matinee price is $5.00. Adults are those over 13 years. The normal children&#39;s
// ticket price is $4.00, however the children&#39;s matinee price is $2.00. Assume
// that a matinee starts at any time earlier than 5 pm (1700).
// Get the information from the user and then use nested if statements to
// determine the ticket price. It is usually a good idea to separate the &quot;information
// gathering phase&quot; (asking the user for age and time) from the &quot;processing
// phase&quot; of a program (deciding on the ticket price). There are many ways in
// which the if statements can be nested. Sketch out a flowchart as you design
// your program.

import java.util.Scanner;
public class ExerciseEight
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("How old are you in years: ");
        int age = scan.nextInt();
        System.out.println("What time is it (hours): ");
        int hour = scan.nextInt();
        
        if (age >= 13) {
            if (hour >= 17) {
                System.out.println("You pay $8.00");
            } else {
                System.out.println("You pay $5.00");
            }
        } else if (age <= 13) {
            if (hour >= 17) {
                System.out.println("You pay $4.00");
            } else {
                System.out.println("You pay $2.00");
            }
        }
    }
}