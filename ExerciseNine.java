// Sales of movie tickets has been dropping! In an effort to attract more viewers,
// the theater has started a new policy charging $4.00 for all tickets sold after
// 2200 (10 pm). However, no children may purchase tickets after that time.
// Modify exercise 8 to implement the new policy.
// Hint: Add logic operators.

import java.util.Scanner;
public class ExerciseNine
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("How old are you in years: ");
        int age = scan.nextInt();
        System.out.println("What time is it (hours): ");
        int hour = scan.nextInt();
        
        if (age >= 13) {
            if (hour >= 17 && hour < 22) {
                System.out.println("You pay $8.00");
            } else if (hour <= 17) {
                System.out.println("You pay $5.00");
            } else {
                System.out.println("New deal! You pay only $4.00!");
            }
        } else if (age <= 13) {
            if (hour >= 17 && hour < 22) {
                System.out.println("You pay $4.00");
            } else {
                System.out.println("You pay $2.00");
            }
        } else {
            System.out.println("Sorry too late young one");
        }
    }
}