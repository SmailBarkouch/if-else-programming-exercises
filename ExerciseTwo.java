// Bob's Discount Bolts charges the following prices:
//       5 cents per bolt
//       3 cents per nut
//       1 cent per washer
// Write a program that asks the user for the number of bolts, nuts, and washers
// in their purchase and then calculates and prints out the total. As an added
// feature, the program checks the order. A correct order must have at least as
// many nuts as bolts and at least twice as many washers as bolts, otherwise the
// order has an error.
// For an error the program writes out &quot;Check the Order: too few nuts&quot; or &quot;Check
// the Order: too few washers&quot; as appropriate. Both error messages are written if
// the order has both errors. If there are no errors the program writes out &quot;Order
// is OK. In all cases the total price in cents (of the specified number of items) is
// written out.
// Number of bolts: 12 Number of nuts: 8 Number of washers:
// 24 Check the Order: too few nuts Total cost: 108

import java.util.Scanner;
public class ExerciseTwo
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Number of bolts: ");
        int boltsNum = scan.nextInt();
        
        System.out.println("Number of nuts: ");
        int nutsNum = scan.nextInt();
        
        System.out.println("Number of washers: ");
        int washersNum = scan.nextInt();
        
        System.out.print("Checking the Order: ");
        if(nutsNum * 2 == boltsNum && washersNum * 2 == boltsNum) {
            System.out.println("Order is OK");
        } else if (nutsNum * 2 != boltsNum && washersNum * 2 != washersNum) {
            System.out.println("too few nuts and washers")
        } else if (washersNum * 2!= boltsNum) {
            System.out.println("too few washers");
        } else {
        System.out.println("too few nuts");
        }
        int cost = (nutsNum * 3) + washersNum + (boltsNum * 5);
        System.out.print("Total cost: " + cost);
    }
}