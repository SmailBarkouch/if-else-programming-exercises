// At the State Fair Pie Eating Contest all contestants in the heavyweight division
// must weigh within 30 pounds of 250 pounds. Write a program that asks for a
// contestant's weight and then says if the contestant is allowed in the contest.

import java.util.Scanner;
public class ExerciseFour
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your weight: ");
        int weight = scan.nextInt();
        
        if (weight >= 220 && weight <= 280) {
            System.out.println("You are allowed!");
        } else {
            System.out.println("You are not allowed!");
        }
    }
}
