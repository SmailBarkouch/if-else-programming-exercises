// Write a program that calculates the wind chill index given the temperature and
// the wind speed.
//       Enter Wind Speed: 15
//       Enter Temperature: 20
//       Wind Chill: 6.218885266083872
// The wind chill index (WCI) is calculated from the wind speed v in miles per
// hour and the temperature temp in Fahrenheit. Three formulas are used,
// depending on the wind speed:
// If wind speed is less than 3 mph then wind chill =
// current temperature. If the current temperature is
// greater than 50° F then wind chill = current temperature
// otherwise, wind chill = 35.74 + 0.6215*temp - 35.75*v 0.16 + 0.4275*temp*v 0.16
// Hint: To calculate v 0.16  use Math.pow()

import java.util.Scanner;
public class ExerciseSix
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter Wind Speed: ");
        int windSpeed = scan.nextInt();
        System.out.println("Enter Temperature: ");
        int temp = scan.nextInt();
        
        if (windSpeed < 3) {
            System.out.println("Wind Chill: " + temp);
        } else if(temp > 50) {
            System.out.println("Wind Chill: " + temp);
        } else {
            double trueChill = 35.74 + (0.6215 * temp) - (35.75 *Math.pow(windSpeed,0.16)) + 0.4275 * temp * Math.pow(windSpeed, 0.16);
            System.out.println("Wind Chill: " + trueChill);
        }
    }
}