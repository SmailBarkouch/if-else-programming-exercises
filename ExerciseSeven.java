// Write a program that asks for your age in years, months, and days and writes
// out your age in seconds. Do this by calculating the number of total days you
// have been alive, then multiply this by the number of hours per day (24), the
// number of minutes per hour (60), and the number of seconds per minute (60).
// Assume that there are 365 days per year (ignore leap years). But correctly
// take account of the different number of days in different months. If the user
// enters 5 for the number of months, add up the number of days in the first 5 months:31 + 28 + 31 + 30 + 31
// A human lifespan is about 2.5 billion seconds (2.5 billion heart-beats). Write
// out what percentage of your expected lifespan you have lived.

import java.util.Scanner;
public class ExerciseSeven
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("How old are you in years: ");
        int years = scan.nextInt();
        System.out.println("Is there any extra months: ");
        int months = scan.nextInt();
        System.out.println("Is there any extra days: ");
        int days = scan.nextInt();
        
        CalculateSeconds(years, months, days);
    }
    
    public static void CalculateSeconds(int x, int y, int z) 
    {
        if (y == 1) {
            z += 31;
        } else if (y == 2) {
            z += 59;
        } else if (y == 3) {
            z += 90;
        } else if (y == 4) {
            z += 120;
        } else if (y == 5) {
            z += 151;
        } else if (y == 6) {
            z += 181;
        } else if (y == 7) {
            z += 212;
        } else if (y == 8) {
            z += 243;
        } else if (y == 9) {
            z += 273;
        } else if (y == 10) {
            z += 304;
        } else if (y == 11) {
            z += 334;
        } else if (y == 12) {
            z += 365;
        }
        
        z = (x * 365) * 24 * 60 * 60;
        System.out.println("Your age in seconds is " + z);
    }
}