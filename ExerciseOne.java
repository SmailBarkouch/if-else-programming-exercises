// During a sale at a store, a 10% discount is applied to purchases over $10.00.
// Write a program that asks for the amount of a purchase, then calculates the
// discounted price. The purchase amount will be input in cents (as an integer):
// Enter amount of purchases: 2000 Discounted price: 1800
// Hint: Use integer arithmetic throughout the program

import java.util.Scanner;
public class ExerciseOne
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter amount of purchases: ");
        int price = scan.nextInt();
        int discount = deductNum(price);
        System.out.print(" Discounted price: " + discount);
    }
    public static int deductNum(int x) {
        return x - (int)((double)x * .10);
    }
}